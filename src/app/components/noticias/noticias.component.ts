import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { IonInfiniteScroll } from '@ionic/angular';
import { Article, Noticia } from 'src/app/interfaces/interfaces';
import { ActionSheetController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { DataLocalService } from 'src/app/services/data-local.service';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.scss'],
})

export class NoticiasComponent implements OnInit {
  slice: number = 4;
  category: string = 'general';

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  @Input() noticias: Article[] = [];

  constructor(private dataService: DataService, public actionSheetController: ActionSheetController, public dataLocalService: DataLocalService) { }

  ngOnInit() {
  }

  async presentActionSheet(noticia) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Borrar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
          this.noticias = this.noticias.filter(noti => noti.title !== noticia.title);
        }
      }, {
        text: 'Compartir',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Favoritos',
        icon: 'heart',
        handler: () => {
          console.log('Favorite clicked');
          this.dataLocalService.guardarNoticia(noticia);
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

}


